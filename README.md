# Attention Insight API Examples

Repository of various Attention Insight API integration examples.

Want to try, [contact us](https://attentioninsight.com/api/)

## Scope:

- [x] [Javascript / NodeJS](./javascript/README.md)
- [x] [Python](./python/README.md)
- [x] [Curl](./curl/README.md)
- [x] [OpenApi](./api-v2-docs.yaml)

## Documentation:

Attention Insight API [documentation](https://syst-ext-api.runwiththelions.eu/api/documentation) with Swagger.


## Backlog

- [x] Added AOI calculation examples
- [x] Added Python example
- [x] Added Curl example
- [x] Added Video api examples
- [x] Added OpenAPI yaml
- [ ] Update Python: contrast and calculate AOI
- [ ] Add status check, fetch an image
- [ ] Add PHP example
- [ ] Add Go example
- [ ] Add Typescript example

## License

Free to clone and use


