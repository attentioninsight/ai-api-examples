import os
import requests
import time

# Constants
API_KEY = 'YOUR_API_KEY'
# Testing
#BASE_URL = "https://syst-ext-api.runwiththelions.eu/api/v2"

# Production
BASE_URL = "https://ext-api.attentioninsight.com/api/v2"
HEADERS_STUDY = {
    "Accept": "application/json",
    "Api-key": API_KEY,
}

HEADERS_INFO = {
    "Accept": "application/json",
    "Content-Type": "application/json",
    "Api-key": API_KEY,
}

OUTPUT_DIR = "output"
RETRY_INTERVAL = 5  # seconds
INITIAL_WAIT_TIME = 5  # seconds
MAX_RETRIES = 30  # Maximum number of retries to wait for study completion


def create_study(study_name: str, study_type: str, content_type: str, tasks: list, image_path: str) -> dict:
    """Create a new study and return the response JSON."""
    url = f"{BASE_URL}/studies"
    payload = {
        "study_name": study_name,
        "study_type": study_type,
        "content_type": content_type,
        "tasks[]": tasks,
    }

    with open(image_path, "rb") as image_file:
        files = {"file": (os.path.basename(image_path), image_file)}
        response = requests.post(url, headers=HEADERS_STUDY, data=payload, files=files)

    if response.status_code == 200:
        return response.json()
    else:
        print(response.json())
        raise ValueError(f"Failed to create study: {response.status_code} - {response.text}")


def create_video_study(study_name: str, study_type: str, tasks: list, video_path: str) -> dict:
    """Create a new video study and return the response JSON."""
    url = f"{BASE_URL}/studies/video"
    payload = {
        "study_name": study_name,
        "study_type": study_type,
        "tasks[]": tasks,
    }

    with open(video_path, "rb") as image_file:
        files = {"file": (os.path.basename(video_path), image_file)}
        response = requests.post(url, headers=HEADERS_STUDY, data=payload, files=files)

    if response.status_code == 200:
        return response.json()
    else:
        print(response.json())
        raise ValueError(f"Failed to create study: {response.status_code} - {response.text}")


def get_study_status(study_id: str) -> dict:
    """Check the status of a study and return the response JSON."""
    url = f"{BASE_URL}/studies/{study_id}"
    print(url)
    response = requests.get(url, headers=HEADERS_INFO)
    if response.status_code == 200:
        return response.json()
    else:
        print(response.json())
        raise ValueError(f"Failed to get study status: {response.status_code} - {response.text}")


def wait_for_study_completion(study_id: str)-> dict:
    """Wait until the study status is 'finished' or the max retries are reached."""
    print(f"Waiting for study {study_id} to complete...")
    time.sleep(INITIAL_WAIT_TIME)

    retries = 0
    while retries < MAX_RETRIES:
        print(f"Checking study status... (Attempt {retries + 1}/{MAX_RETRIES})")
        status_response = get_study_status(study_id)
        if "data" in status_response and "status" in status_response["data"]:
            status = status_response["data"]["status"]
            if status == "finished":
                print("Study processing finished.")
                return status_response["data"]
            else:
                print(f"Study status: {status}. Retrying in {RETRY_INTERVAL} seconds... (Attempt {retries + 1}/{MAX_RETRIES})")
                time.sleep(RETRY_INTERVAL)
                retries += 1
        else:
            raise ValueError("Unexpected response format while checking study status:", status_response)
    
    raise TimeoutError(f"Max retries reached ({MAX_RETRIES}). Study did not finish in time.")


def download_image(study_id: str, image_type: str, output_path: str):
    """Download an image of a given type for a study."""
    url = f"{BASE_URL}/studies/{study_id}/image?image={image_type}"
    response = requests.get(url, headers=HEADERS_INFO)
    if response.status_code == 200:
        with open(output_path, "wb") as file:
            file.write(response.content)
        print(f"{image_type.capitalize()} image saved successfully to {output_path}!")
    else:
        raise ValueError(f"Failed to retrieve {image_type} image: {response.status_code} - {response.text}")

def download_video(study_id: str, video_type: str, output_path: str):
    """Download an image of a given type for a study."""
    url = f"{BASE_URL}/studies/{study_id}/video?video={video_type}"
    response = requests.get(url, headers=HEADERS_INFO)
    if response.status_code == 200:
        with open(output_path, "wb") as file:
            file.write(response.content)
        print(f"{video_type.capitalize()} image saved successfully to {output_path}!")
    else:
        raise ValueError(f"Failed to retrieve {video_type} image: {response.status_code} - {response.text}")


def parse_clarity_score(study_data: dict):
    """Parse the clarity score and description from the study data."""
    if "aesthetics" in study_data:
        aesthetics = study_data["aesthetics"]
        
        # Extract clarity_score
        clarity_score = aesthetics.get("clarity_score")
        
        # Extract clarity_description
        clarity_description = aesthetics.get("clarity_description")
        
        if clarity_score is not None and clarity_description is not None:
            print(f"- Clarity score: {clarity_score}")
            print(f"- Clarity description: {clarity_description}")
        else:
            raise ValueError("Clarity score or description not found in aesthetics data.")
    else:
        raise ValueError("Aesthetics data not found in study data.")

def study_with_focus_clarity():
    """Main function to create a study, monitor its status, and download results."""
    try:
        # Define the image path
        image_path = os.path.join(os.getcwd(), "example/example.png")

        # Create a new study
        study_response = create_study("test", "web", "business", ["clarity_score", "focus"], image_path)
        study_id = study_response.get("data", {}).get("study_id")
        if not study_id:
            raise ValueError("Study ID not found in response.")

        print(f"Study created successfully with ID: {study_id}")

        # Wait for study to complete
        study = wait_for_study_completion(study_id)
        
        # Parse the clarity score
        parse_clarity_score(study)

        # Download and save images
        os.makedirs(OUTPUT_DIR, exist_ok=True)
        download_image(study_id, "heatmap", os.path.join(OUTPUT_DIR, "heatmap.jpg"))
        download_image(study_id, "focus", os.path.join(OUTPUT_DIR, "focus.jpg"))

    except ValueError as e:
        print(f"Error: {e}")
    except TimeoutError as e:
        print(f"Timeout: {e}")
    except Exception as e:
        print(f"An unexpected error occurred: {e}")

def study_only_heatmap():
    """Main function to create a study, monitor its status, and download results."""
    try:
        # Define the image path
        image_path = os.path.join(os.getcwd(), "example/example.png")

        # Create a new study
        study_response = create_study("test", "web", "business", [], image_path)
        study_id = study_response.get("data", {}).get("study_id")
        if not study_id:
            raise ValueError("Study ID not found in response.")

        print(f"Study created successfully with ID: {study_id}")

        # Wait for study to complete
        wait_for_study_completion(study_id)

        # Download and save images
        os.makedirs(OUTPUT_DIR, exist_ok=True)
        download_image(study_id, "heatmap", os.path.join(OUTPUT_DIR, "heatmap.jpg"))

    except ValueError as e:
        print(f"Error: {e}")
    except TimeoutError as e:
        print(f"Timeout: {e}")
    except Exception as e:
        print(f"An unexpected error occurred: {e}")


def video_study_only_heatmap():
    """Main function to create a study, monitor its status, and download results."""
    try:
        # Define the image path
        image_path = os.path.join(os.getcwd(), "example/example.mp4")

        # Create a new study
        study_response = create_video_study("test", "general", [], image_path)
        study_id = study_response.get("data", {}).get("study_id")
        if not study_id:
            raise ValueError("Study ID not found in response.")

        print(f"Study created successfully with ID: {study_id}")

        # Wait for study to complete
        wait_for_study_completion(study_id)

        # Download and save images
        os.makedirs(OUTPUT_DIR, exist_ok=True)
        download_video(study_id, "heatmap", os.path.join(OUTPUT_DIR, "heatmap.mp4"))

    except ValueError as e:
        print(f"Error: {e}")
    except TimeoutError as e:
        print(f"Timeout: {e}")
    except Exception as e:
        print(f"An unexpected error occurred: {e}")

if __name__ == "__main__":
    # Run the main tasks
    #study_with_focus_clarity()

    # Run the heatmap only
    #study_only_heatmap()
    video_study_only_heatmap()