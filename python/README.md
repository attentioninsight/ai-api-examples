# Example in Python

Simple create study python example

# Run

Requires: Python 3.10 (p.s. lower version not tested)

1. Set API in api.py
2. Run `python python/api.py`

# Note
Test environment requires separate account and API KEY. Contact support for more.
