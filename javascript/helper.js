/**
 * Retrieves the binary data of an image from the specified URL.
 * @async
 * @param {string} url - The URL of the image.
 * @returns {Promise<Buffer>} - A Promise that resolves to a Buffer object containing the binary data of the image.
 */
export async function getFileBinary(url) {
  // Use the Fetch API to make an HTTP request for the image at the specified URL.
  const response = await fetch(url);
  // Get a Blob object from the response data.
  const blob = await response.blob();
  // Convert the Blob object to an ArrayBuffer.
  return blob;
}

// sleep function
export function sleep(ms) {
  console.log("Waiting for " + ms + "ms ...")

  return new Promise((resolve) => setTimeout(resolve, ms));
}


/*
Helper function, helps convert pixel coordinates to percentage coordinates in float
*/
export function convertToPercentageCoordinates(pxX, pxY, pxWidth, pxHeight, imgWidth, imgWeight) {
  const percentageX = (pxX / imgWidth) * 100;
  const percentageY = (pxY / imgWeight) * 100;
  const percentageWidth = (pxWidth / imgWidth) * 100;
  const percentageHeight = (pxHeight / imgWeight) * 100;
  return { x: percentageX, y: percentageY, width: percentageWidth, height: percentageHeight };
}

/*
Helper function, helps convert percentage coordinates to pixel coordinates and round them to integer
*/
export function convertToPixelCoordinates(percentageX, percentageY, percentageWidth, percentageHeight, imgWidth, imgWeight) {
  const pxX = Math.round((percentageX / 100) * imgWidth);
  const pxY = Math.round((percentageY / 100) * imgWeight);
  const pxWidth = Math.round((percentageWidth / 100) * imgWidth);
  const pxHeight = Math.round((percentageHeight / 100) * imgWeight);
  return { x: pxX, y: pxY, width: pxWidth, height: pxHeight };
}
