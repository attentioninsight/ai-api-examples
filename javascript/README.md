# Example in Javascript

Simple create study javascript example

# Run

Requires: Node v.18 (p.s. lower version not tested)

1. Build: `npm install`
2. Set API: `export API_KEY=<api-key>`
3. Run `node main.js`

# Note

Set `NODE_ENV=production` if you want to use production env