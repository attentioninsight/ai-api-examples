import { getEnv } from "./constants.js";

let headers = {
  Accept: "application/json",
  "Api-key": process.env.API_KEY,
};

const env = getEnv();

// Create Study
export async function createStudy(data) {
  console.log("\n[*] Creating study...\n");
  // Validate inputs
  if (
    !data.studyName ||
    !data.studyType ||
    !data.contentType ||
    !data.file ||
    !data.filename ||
    !data.tasks ||
    !data.public_shared
  ) {
    throw new Error("Invalid input data");
  }

  const formData = new FormData();
  formData.append("study_name", data.studyName);
  formData.append("study_type", data.studyType);
  formData.append("content_type", data.contentType);
  formData.append("file", data.file, data.filename);
  data.tasks.forEach((task) => formData.append("tasks[]", task));
  formData.append("public_shared", data.public_shared);

  let studyData = null;

  await fetch(env.DOMAIN + "studies/", {
    headers,
    method: "POST",
    body: formData,
  })
    .then((response) =>
      response.json().then((data) => ({
        status: response.status,
        data: data,
      }))
    )
    .then((data) => (studyData = data))
    .catch((error) => console.log(error));

  if (studyData.status === 200) {
    console.log("Study created successfully", studyData);
    return studyData.data.data.study_id;
  } else {
    console.error("Study creation failed", studyData);
    return null;
  }
}

// Create Study
export async function createVideoStudy(data) {
  console.log("\n[*] Creating video study...\n");
  // Validate inputs
  if (
    !data.studyName ||
    !data.studyType ||
    !data.file ||
    !data.filename ||
    !data.tasks ||
    !data.public_shared
  ) {
    throw new Error("Invalid input data");
  }

  const formData = new FormData();
  formData.append("study_name", data.studyName);
  formData.append("study_type", data.studyType);
  formData.append("file", data.file, data.filename);
  data.tasks.forEach((task) => formData.append("tasks[]", task));
  formData.append("public_shared", data.public_shared);

  let studyData = null;

  await fetch(env.DOMAIN + "studies/video", {
    headers,
    method: "POST",
    body: formData,
  })
    .then((response) =>
      response.json().then((data) => ({
        status: response.status,
        data: data,
      }))
    )
    .then((data) => (studyData = data))
    .catch((error) => console.log(error));

  if (studyData.status === 200) {
    console.log("Video study created successfully", studyData);
    return studyData.data.data.study_id;
  } else {
    console.error("Video study creation failed", studyData);
    return null;
  }
}

// Check processing status
export async function checkStatus(studyId) {
  console.log("\n[*] Checking study status...\n");

  const url = env.DOMAIN + "studies/" + studyId + "/status";

  return await fetch(url, { headers })
    .then(async (response) => {
      const status = await response.json();
      return status;
    })
    .catch((error) => {
      console.error("Error checking status: ", error);
      return null;
    });
}

// Fetch study data
export async function fetchStudyData(studyId) {
  console.log("\n[*] Creating study data...\n");

  const url = env.DOMAIN + "studies/" + studyId;

  return await fetch(url, { headers })
    .then(async (response) => {
      const status = await response.json();
      return status;
    })
    .catch((error) => {
      console.error("Error checking status: ", error);
      return null;
    });
}

// Fetch analysis images
export async function fetchStudyImage(studyId, type) {
  console.log(`\n[*] Creating study image ${type}...\n`);

  const validTypes = ["heatmap", "focus", "saliency", "aoi", "aoi_heatmap"];

  if (!validTypes.includes(type)) {
    console.error("Invalid requested image type");
    return { status: null, image: null };
  }

  const url = env.DOMAIN + "studies/" + studyId + "/image?image=" + type;

  return await fetch(url, { headers })
    .then(async (response) => {
      if (response.status === 200) {
        const image = await response.arrayBuffer();
        return image;
      } else {
        return null;
      }
    })
    .catch((error) => {
      console.error("Error fetching image: ", error);
      return null;
    });
}

// Fetch analysis video
export async function fetchStudyVideo(studyId, type) {
  console.log(`\n[*] Creating study image ${type}...\n`);

  const validTypes = ["heatmap", "focus", "saliency", "aoi_heatmap"];

  if (!validTypes.includes(type)) {
    console.error("Invalid requested video type");
    return { status: null, image: null };
  }

  const url = env.DOMAIN + "studies/" + studyId + "/video?video=" + type;

  return await fetch(url, { headers })
    .then(async (response) => {
      if (response.status === 200) {
        const image = await response.arrayBuffer();
        return image;
      } else {
        return null;
      }
    })
    .catch((error) => {
      console.error("Error fetching video: ", error);
      return null;
    });
}


// API to post creation AOI for a study
// Calculation is async
export async function createAOI(studyId, data, render = false) {
  console.log("\n[*] Creating AOIs for study...\n");

  // Validate if data has at least one aois
  if (
    !data.aois && length(data.aois) < 1
  ) {
    throw new Error("Invalid input data");
  }
  // if render true, then add query param to render image ?render
  const url = env.DOMAIN + "studies/" + studyId + "/aoi" + (render ? "?render" : "");

  headers["Content-Type"] = "application/json";

  return await fetch(url, {
    headers,
    method: "POST",
    body: JSON.stringify(data)
  })
    .then(async (response) => {
      const status = await response.json();
      return status;
    })
    .catch((error) => {
      console.error("Error creating AOI: ", error);
      return null;
    });
}

// Get AOI for a study
export async function getAOI(studyId) {
  console.log("\n[*] Getting AOIs for study...\n");

  const url = env.DOMAIN + "studies/" + studyId + "/aoi";

  return await fetch(url, { headers })
    .then(async (response) => {
      const status = await response.json();
      return status;
    })
    .catch((error) => {
      console.error("Error getting AOI: ", error);
      return null;
    });
}
