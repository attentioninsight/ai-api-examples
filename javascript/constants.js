const DEVELOPMENT_ENV = {
  DOMAIN: "https://dev-ext-api.runwiththelions.eu/api/v2/",
  ENV: "development",
};

const PRODUCTION_ENV = {
  DOMAIN: "https://ext-api.attentioninsight.com/api/v2/",
  ENV: "production",
};

export function getEnv() {
  if (process.env.NODE_ENV === "production") {
    return PRODUCTION_ENV;
  } else {
    return DEVELOPMENT_ENV;
  }
}
