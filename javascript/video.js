import { getFileBinary} from "./helper.js";
import {
  fetchStudyData,
  checkStatus,
  fetchStudyVideo,
  createVideoStudy,
} from "./api.js";

import { fileURLToPath } from 'url';
import { dirname, join } from 'path';
import { writeFile } from 'fs/promises';

const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

// Post Heatmap Data from the client
const analyze = async () => {
  const videoUrl =
    "https://storage.googleapis.com/attention-insight-public/example/example.mp4";

  const video = await getFileBinary(videoUrl);

  /*
   * studyType:
   *  - general - for generic videos
   *
   * additional tasks:
   * - focus - generate focus map
   * - auto_aoi - try detect AOI automatically
   */

  const studyId = await createVideoStudy({
    studyName: "Study",
    studyType: "general",
    file: video,
    filename: "example.mp4",
    tasks: ["focus", "auto_aoi"],
    public_shared: 1,
  });

  if (studyId == null) {
    console.error("Cancelling, no study created yet");
    return;
  }

  // Check status
  if (!(await waitForCompletion(studyId))) {
    console.error("Cancelling, study processing failed");
    return;
  }

  // Fetch study data
  const data = await fetchStudyData(studyId);
  if (data != null) {
    console.log("Study data:", data);
  }

  // Fetch heatmap image
  const arrayBuffer = await fetchStudyVideo(studyId, "heatmap");

  if (arrayBuffer !== null) {
    console.log("Image fetched successfully");
    
    const filePath = join(__dirname, '..', 'output', 'heatmap.mp4');
    const buffer = Buffer.from(arrayBuffer);
    await writeFile(filePath, buffer);

  } else {
    console.error("Image fetch failed, try later again");
  }
  return studyId;
};


async function waitForCompletion(studyId) {
  console.log("processing... please wait");
  let completed = false;
  while (!completed) {
    const processingResponse = await checkStatus(studyId);

    if (processingResponse.data.status === "finished") {
      console.log("allTasksDone", processingResponse.data.tasks);
      const allTasksDone = Object.values(processingResponse.data.tasks).every(
        (taskStatus) => taskStatus === "done" || taskStatus === "not requested"
      );
      if (allTasksDone) {
        completed = true;
      }
    }

    await new Promise((resolve) => setTimeout(resolve, 5000)); // Wait for 3 second before checking again
  }

  console.log("Study processing completed");
  return completed;
}

// Create analysis and get results with heatmap
const studyId = await analyze();

