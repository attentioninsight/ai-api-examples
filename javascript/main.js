import { getFileBinary, sleep, convertToPercentageCoordinates, convertToPixelCoordinates } from "./helper.js";
import {
  createStudy,
  fetchStudyImage,
  fetchStudyData,
  checkStatus,
  createAOI,
  getAOI,
} from "./api.js";

import { fileURLToPath } from 'url';
import { dirname, join } from 'path';
import { writeFile } from 'fs/promises';

const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

// Post Heatmap Data from the client
const analyze = async () => {
  const imageUrl =
    "https://storage.googleapis.com/attention-insight-public/example/example.png";

  const image = await getFileBinary(imageUrl);

  /*
   * studyType:
   *  - web - best desktop web pages designs
   *  - mobile - best mobile web pages designs
   *  - general - for common marketing materials
   *  - poster - best for posters designs
   *  - packaging - best for packaging designs
   *  - shelves - best to test products on shelves
   *
   * contentType (for web studyType only):
   * - business - for landing web page designs ()
   * - news_sites - for news portal designs
   * - online_shops - for ecommerce designs
   * - general - for more generic web pages
   *
   * contentType (for all others):
   * - general - for generic designs
   * 
   * additional tasks:
   * - focus - generate focus map
   * - clarity_score - generate clarity score
   * - auto_aoi - try detect AOI automatically
   * - contrast - generate contrast map
   */

  const studyId = await createStudy({
    studyName: "Study",
    studyType: "web",
    contentType: "business",
    file: image,
    filename: "example.png",
    tasks: ["focus", "clarity_score", "auto_aoi"],
    public_shared: 1,
  });

  if (studyId == null) {
    console.error("Cancelling, no study created yet");
    return;
  }

  // Check status
  if (!(await waitForCompletion(studyId))) {
    console.error("Cancelling, study processing failed");
    return;
  }

  // Fetch study data
  const data = await fetchStudyData(studyId);
  if (data != null) {
    console.log("Study data:", data);
  }

  // Fetch heatmap image
  const arrayBuffer = await fetchStudyImage(studyId, "heatmap");

  if (arrayBuffer !== null) {
    console.log("Image fetched successfully");
    
    const filePath = join(__dirname, '..', 'output', 'heatmap.png');
    const buffer = Buffer.from(arrayBuffer);
    await writeFile(filePath, buffer);

  } else {
    console.error("Image fetch failed, try later again");
  }
  return studyId;
};

const calculateAoi = async (studyId) => {
  // Create two AOIs for study
  // NOTES: AOI coordinates are in percentages in float format
  const postData = {
    "aois": [
      {
          "geometry": {
              "x": 76,
              "y": 1,
              "type": "RECTANGLE",
              "width": 8,
              "height": 6
          },
          "name": "Primary CTA"
      },
      {
        "geometry": {
            "x": 27,
            "y": 57,
            "type": "RECTANGLE",
            "width": 45,
            "height": 13
        },
        "name": "Heading"
    }
    ]
  }

  // create AOI without rendering on images
  const response = await createAOI(studyId, postData, false);
  console.log("AOI response: ", response);
  
  //wait for 5 seconds
  await sleep(5000);

  const aois = await getAOI(studyId);

  console.log("List of ", JSON.stringify(aois));

  
}

const calculateAoiWithRender = async (studyId) => {
  // Create two AOIs for study
  // NOTES: AOI coordinates are in percentages in float format
  const postData = {
    "aois": [
      {
          "geometry": {
              "x": 76,
              "y": 1,
              "type": "RECTANGLE",
              "width": 8,
              "height": 6
          },
          "name": "Primary CTA"
      },
    ]
  }

  // create AOI with rendering on images
  const response = await createAOI(studyId, postData, true);
  console.log("AOI response: ", response);
  
  //wait for 5 seconds
  await sleep(10000);

  // Fetch heatmap image
  const arrayBuffer = await fetchStudyImage(studyId, "aoi_heatmap");

  if (arrayBuffer !== null) {
    console.log("Image fetched successfully");
    // write blob to output file
    
    const filePath = join(__dirname, '..', 'output', 'aoi_heatmap.png');
    const buffer = Buffer.from(arrayBuffer);
    await writeFile(filePath, buffer);

  } else {
    console.error("Image fetch failed, try later again");
  }
}

async function waitForCompletion(studyId) {
  console.log("processing... please wait");
  let completed = false;
  while (!completed) {
    const processingResponse = await checkStatus(studyId);

    if (processingResponse.data.status === "finished") {
      const allTasksDone = Object.values(processingResponse.data.tasks).every(
        (taskStatus) => taskStatus === "done" || taskStatus === "not requested"
      );
      if (allTasksDone) {
        completed = true;
      }
    }

    await new Promise((resolve) => setTimeout(resolve, 3000)); // Wait for 3 second before checking again
  }

  console.log("Study processing completed");
  return completed;
}

function coordinatesConvertion() {
  console.log("[*] Coordinates convertion demo");
  // initial values in PX
  const width = 1920;
  const height = 1080;
  const x = 10;
  const y = 20;
  const w = 50;
  const h = 15;

  console.log("Initial values: ", {x, y, w, h, width, height});

  const resultPrntg = convertToPercentageCoordinates(x, y, w, h, width, height);
  console.log("Result in percentages: ", resultPrntg);

  const resultPx = convertToPixelCoordinates(resultPrntg.x, resultPrntg.y, resultPrntg.width, resultPrntg.height, width, height);
  console.log("Result in pixels: ", resultPx);

}


// Create analysis and get results with heatmap
const studyId = await analyze();

// Create two AOIs and get aoi data
await calculateAoi(studyId);

// Create AOI and draw on heatmap
await calculateAoiWithRender(studyId);

// Demo for coordinates convertion
coordinatesConvertion();
