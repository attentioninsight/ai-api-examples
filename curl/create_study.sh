#!/bin/bash

if [ -z "$API_KEY" ]; then
  echo "Error: API_KEY environment variable is not set."
  exit 1
fi

# Accept the file path as a parameter
file="$1"

# Check file path
if [ -z "$file" ]; then
  echo "Usage: $0 <file_path>"
  exit 1
fi

curl --request POST \
--url https://ext-api.attentioninsight.com/api/v2/studies \
--header 'Accept: application/json' \
--header "Api-key: ${API_KEY}" \
--header 'Content-Type: multipart/form-data' \
--form study_name=Test \
--form study_type=web \
--form content_type=business \
--form "file=@${file}" \
--form 'tasks[]="focus"' \
--form 'tasks[]="clarity_score"' \
--form 'tasks[]="auto_aoi"' \
--form 'tasks[]="contrast"' \
--form public_shared=0

