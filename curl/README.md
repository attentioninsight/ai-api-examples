# Example in Curl

Simple create study with curl command

# Run

0. chmod +x *.sh
1. Set you key `export API_KEY=<api_key>`
2. Create study `./create_study.sh ../example/example.png`
3. Get study info `./get_study_info.sh <study_id> `
4. Download heatmap `./get_heatmap.sh <study_id>`

# Note
Test environment requires separate account and API KEY. Contact support for more.
