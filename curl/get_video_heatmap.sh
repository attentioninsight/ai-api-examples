#!/bin/bash

# Accept the study ID as a parameter
studyId="$1"

# Check if studyId is provided
if [ -z "$studyId" ]; then
  echo "Usage: $0 <studyId>"
  exit 1
fi

# Check if API_KEY environment variable is set
if [ -z "$API_KEY" ]; then
  echo "Error: API_KEY environment variable is not set."
  exit 1
fi

# Make the curl request with the studyId parameter
curl --request GET \
--url "https://ext-api.attentioninsight.com/api/v2/studies/${studyId}/video?video=heatmap" \
--header 'Accept: application/json' \
--header "Api-key: ${API_KEY}" \
--output output_video.mp4
